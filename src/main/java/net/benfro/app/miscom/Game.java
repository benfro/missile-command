package net.benfro.app.miscom;

import java.awt.*;

/**
 * Game interface.
 * 
 * @author benfro
 */
public interface Game {

    void draw(Graphics screen);

    void start();
    
}
