package net.benfro.app.miscom;

import net.benfro.app.miscom.sprite.*;
import net.benfro.app.miscom.sprite.Building.Builder;
import net.benfro.app.miscom.util.BoundedRandomPointGenerator;
import net.benfro.app.miscom.util.Factory;
import net.benfro.app.miscom.util.GraphicsUtil;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static net.benfro.app.miscom.MissileCommandSettings.ICBM_MAX_SPAWN_WAIT_MILLISECONDS;
import static net.benfro.app.miscom.MissileCommandSettings.ICBM_MIN_SPAWN_WAIT_MILLISECONDS;

/**
 * Functions as a model for the entire game, holds all game sprites, is
 * responsible for rendering, spawning of missiles etc.
 *
 * @author benfro
 */
public class MissileCommand implements Game {

    /**
     * Missile factory for factoring Intercontinental Ballistic Missiles (ICBM)
     * Missiles are given a target rectangle of the owning component to aim for.
     * They are spawned on top of the component at ([randomX, 0]
     */
    public static class ICBMFactory implements Factory<MissileSprite> {

        private final BoundedRandomPointGenerator pointGenerator =
                new BoundedRandomPointGenerator();

        private Dimension owningComponent;

        public ICBMFactory(Dimension owningComponent) {
            this.owningComponent = owningComponent;
        }

        public MissileSprite newInstance() {

            double componentHeight = owningComponent.getHeight();
            double componentWidth = owningComponent.getWidth();

            Point2D.Float upperLeft =
                    GraphicsUtil.toPoint(0, componentHeight - 100f);
            Point2D.Float netherRight =
                    GraphicsUtil.toPoint(componentWidth, componentHeight);
            return new MissileSprite.Builder()
                    .endX(pointGenerator.generateXCoordinate(upperLeft, netherRight))
                    .endY(pointGenerator.generateYCoordinate(upperLeft, netherRight))
                    .startX(pointGenerator.generateXCoordinate(0, (float) componentWidth))
                    .startY(0.0f)
                    .speed(2)
                    .build();
        }
    }

    /**
     * This Runnable is responsible for spawning ICBM during the play It does so
     * with a time interval between ICBM_MAX_SPAWN_WAIT_MILLISECONDS and
     * ICBM_MIN_SPAWN_WAIT_MILLISECONDS
     */
    private class EnemyRandomizer implements Runnable {

        Random r = new Random();

        public void run() {
            while (true) {
                int sleepFor = r.nextInt(ICBM_MAX_SPAWN_WAIT_MILLISECONDS)
                        + ICBM_MIN_SPAWN_WAIT_MILLISECONDS;
                try {
                    Thread.sleep(sleepFor);
                    MissileSprite missile = ICBMFactory.newInstance();
                    ICBMissiles.add(missile);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MissileCommand.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    };
    private Factory<MissileSprite> ICBMFactory;

    /**
     * Holds every ICBM in game
     */
    private final List<MissileSprite> ICBMissiles;

    /**
     * Holds every ACM in game
     */
    private final List<MissileSprite> ACMissiles;

    /**
     * Holds missile platforms, building and other static sprites
     */
    private final List<AbstractGameSprite> staticSprites;

    /**
     * Constructor for testability purposes
     */
    MissileCommand() {
        ICBMissiles = Collections.synchronizedList(new ArrayList<MissileSprite>());
        ACMissiles = Collections.synchronizedList(new ArrayList<MissileSprite>());
        staticSprites = Collections.synchronizedList(new ArrayList<AbstractGameSprite>());
    }

    public MissileCommand(Dimension sceneDimension) {
        this();
        ICBMFactory = new ICBMFactory(sceneDimension);
    }

    @Override
    public final void start() {
        new Thread(new EnemyRandomizer()).start();
    }

    <T extends MissileSprite> void addACM(T missile) {
        synchronized (ACMissiles) {
            ACMissiles.add(missile);
        }
    }

    void addStaticSprite(AbstractGameSprite platform) {
        this.staticSprites.add(platform);
    }

    public List<MissileSprite> getACMissiles() {
        return ACMissiles;
    }

    public List<AbstractGameSprite> getStaticSprites() {
        return staticSprites;
    }

    public Factory<MissileSprite> getICBMFactory() {
        return ICBMFactory;
    }

    /**
     * Fire an Anti Ballistic Missile This method handles mouse click from the
     * player
     *
     * @param aimingPoint
     */
    public void fireABM(Point2D.Float aimingPoint) {
        AbstractGameSprite platform = null;
        // Simplest possible firing strategy - select a platform with missiles left
        synchronized (staticSprites) {
            for (AbstractGameSprite gameSprite : staticSprites) {
                if (gameSprite instanceof MissilePlatform) {
                    if (((MissilePlatform) gameSprite).getModel().getNumberOfMissiles() > 0) {
                        platform = gameSprite;
                    }
                }
            }
        }
        // If there is a platfom, fire an ACM
        if (platform != null) {
            MissileSprite acmMissile = ((MissilePlatform) platform).spawnACMissile(aimingPoint);
            addACM(acmMissile);
        }
    }

    /**
     * This method drives the game. Collision detect, updating and rendering in
     * handled here in a couple of synchronized blocks.
     */
    public void draw(Graphics screen) {

        // Collision detect ICBM city hits
        collisionDetectBuildingsInternal(staticSprites, ICBMissiles);
        // Collision detect ACMs on ICBMs
        collisionDetectMissiles(ICBMissiles, ACMissiles);
        // Collision detect cascading ICBM explosions
        collisionDetectMissiles(ICBMissiles, ICBMissiles);
        // Update graphics and draw()
        moveAndDrawInternal(screen, ICBMissiles);
        moveAndDrawInternal(screen, ACMissiles);
        moveAndDrawInternal(screen, staticSprites);
        // Clear all spent missiles from game
        clearMissileListFromDestroyed(ICBMissiles);
        clearMissileListFromDestroyed(ACMissiles);

    }

    /**
     * Visible for testability
     *
     * @param runners
     * @param exploders
     */
    void collisionDetectMissiles(List<MissileSprite> runners, List<MissileSprite> exploders) {

        synchronized (exploders) {
            for (MissileSprite exploder : exploders) {
                for (MissileSprite runner : runners) {
                    if (exploder.isCollision(runner)
                            && exploder.isExploding()
                            && !runner.isExploding()) {
                        runner.onCollision();
                    }
                }
            }
        }
    }

    private void collisionDetectBuildingsInternal(List<AbstractGameSprite> staticSpriteList, List<MissileSprite> missileList) {
        synchronized (missileList) {
            for (AbstractGameSprite staticSprite : staticSpriteList) {
                for (MissileSprite missileSprite : missileList) {
                    if (staticSprite.isCollision(missileSprite)) {
                        missileSprite.onCollision();
                        staticSprite.onCollision();
                    }
                }
            }
        }
    }

    private void moveAndDrawInternal(Graphics screen, List<? extends AbstractGameSprite> spriteList) {
        synchronized (spriteList) {
            for (AbstractGameSprite gameSprite : spriteList) {
                if (gameSprite instanceof Moveable) {
                    ((Moveable) gameSprite).move();
                }
                gameSprite.draw(screen);
            }
        }
    }

    private void clearMissileListFromDestroyed(List<MissileSprite> missileList) {
        List<MissileSprite> missilesToRemove = new ArrayList<MissileSprite>();
        synchronized (missileList) {
            for (MissileSprite missile : missileList) {
                if (missile.isDestroyed()) {
                    missilesToRemove.add(missile);
                }
            }
            missileList.removeAll(missilesToRemove);
        }
    }

    /**
     * Build a default game scene
     */
    public static class GameFactory implements Factory<MissileCommand> {

        /**
         * The rectangle of the Swing component
         */
        private Dimension sceneDimension;

        public GameFactory(Dimension sceneDimension) {
            this.sceneDimension = sceneDimension;
        }

        public MissileCommand newInstance() {
            MissileCommand game = new MissileCommand(sceneDimension);

            MissilePlatform platform1 = createMissilePlatform(10, 50, 10, 40, 35, Color.yellow);
            game.addStaticSprite(platform1);

            MissilePlatform platform2 = createMissilePlatform(280, 50, 10, 40, 35, Color.yellow);
            game.addStaticSprite(platform2);

            MissilePlatform platform3 = createMissilePlatform(560, 50, 10, 40, 35, Color.yellow);
            game.addStaticSprite(platform3);

            for (int i = 0; i < 5; i++) {
                Point2D.Float handle = GraphicsUtil.toPoint(65 + 45 * i, sceneDimension.height - 65);
                Building.Builder b = new Builder();
                b.color(Color.LIGHT_GRAY).health(5).height(55).width(20).handle(handle);;
                game.addStaticSprite(b.build());
            }
            
            for (int i = 0; i < 5; i++) {
                Point2D.Float handle = GraphicsUtil.toPoint(335 + 45 * i, sceneDimension.height - 65);
                Building.Builder b = new Builder();
                b.color(Color.LIGHT_GRAY).health(5).height(55).width(20).handle(handle);;
                game.addStaticSprite(b.build());
            }
            return game;
        }

        private MissilePlatform createMissilePlatform(int handleXCoord,
                int handleYCoord, int numMissiles, int height, int width, Color color) {
            return new MissilePlatform.Builder()
                    .dimensionHeight(height)
                    .dimensionWidth(width)
                    .handle(GraphicsUtil.toPoint(handleXCoord, sceneDimension.height - handleYCoord))
                    .color(color)
                    .numberOfMissiles(numMissiles)
                    .build();
        }
    }
}
