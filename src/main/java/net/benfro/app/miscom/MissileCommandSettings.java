package net.benfro.app.miscom;

import java.awt.*;

/**
 * Contains default setting constants to be statically imported into the game.
 * Could be replaced by a .properties file or similar.
 *
 * @author benfro
 */
public interface MissileCommandSettings {
    /*************************************************
     * ICBM settings
     *************************************************/

    public static final int ICBM_MAX_SPAWN_WAIT_MILLISECONDS = 5000;

    public static final int ICBM_MIN_SPAWN_WAIT_MILLISECONDS = 500;

    public static final Color ICBM_COLOR = Color.RED;
    
    public static final Color ICBM_TRAJECTORY_COLOR = Color.ORANGE;

    public static final int ICBM_HEIGHT = 6;

    public static final int ICBM_WIDTH = 6;
    
    /*************************************************
     * ABM settings
     *************************************************/

    public static final Color ACM_COLOR = Color.BLUE;
    
    public static final Color ABM_TRAJECTORY_COLOR = Color.ORANGE;

    public static final int ACM_HEIGHT = 40;

    public static final int ACM_WIDTH = 30;

    /*************************************************
     * MissilePlatform setting
     *************************************************/
    
    public static final int PLATFORM_NUM_MISSILES = 10;

    public static final int PLATFORM_HEIGHT = 40;

    public static final int PLATFORM_WIDTH = 30;

    public static final Color PLATFORM_COLOR = Color.GREEN;
//    public static final int ICBM_MAX_SPAWN_WAIT_MILLISECONDS = 5000;
//    public static final int ICBM_MAX_SPAWN_WAIT_MILLISECONDS = 5000;

    /*************************************************
     * Building settings
     *************************************************/
    
}
