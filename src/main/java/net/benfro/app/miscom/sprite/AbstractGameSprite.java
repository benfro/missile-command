package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.util.GraphicsUtil;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.geom.Rectangle2D;
import java.util.UUID;

/**
 * Represents a graphical object on the screen
 *
 * @author benfro
 */
public abstract class AbstractGameSprite<MODEL_CLASS> implements Collidible, ImpactDetectable {

    /**
     * The model instance
     */
    private MODEL_CLASS model;

    /**
     */
    protected Point2D.Float centerPoint;

    /**
     * Dimension object that enclose this GameSprite
     */
    protected Rectangle2D.Float rectangle;

    /**
     * The screen of this sprite when rendered
     */
    protected Color color;

    /**
     * A way to uniquely identify this missile in a Set
     */
    private UUID id;

    private AbstractGameSprite(MODEL_CLASS model, Float centerpoint, Rectangle2D.Float rectangle) {
        this.model = model;
        this.centerPoint = centerpoint;
        this.rectangle = rectangle;
        this.id = UUID.randomUUID();
    }

    private AbstractGameSprite(MODEL_CLASS model, Float centerPoint, float height, float width) {
        this(model, centerPoint, GraphicsUtil.toRectangle(centerPoint, width, height));
    }

    protected AbstractGameSprite(MODEL_CLASS model, Float handle, int height, int width) {
        this(model, handle, GraphicsUtil.toRectangle(handle, width, height));
    }

    protected AbstractGameSprite(MODEL_CLASS model, Float handle, Rectangle2D.Float rectangle, Color color) {
        this(model, handle, rectangle);
        this.color = color;
    }

    /**
     * Draw this sprite on screen
     */
    public abstract void draw(Graphics g);

    public MODEL_CLASS getModel() {
        return model;
    }

    public Point2D.Float getCenterPoint() {
        return centerPoint;
    }

    public Rectangle2D.Float getRectangle() {
        return rectangle;
    }

    protected void setModel(MODEL_CLASS model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    /**
     * Examine whether the provided point is inside this sprites dimension
     * rectangle
     *
     * @param point
     * @return
     */
    public final boolean isInside(Point2D.Float point) {
        return rectangle.contains(point);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractGameSprite<MODEL_CLASS> other = (AbstractGameSprite<MODEL_CLASS>) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }

    public void setCenterPoint(Point2D.Float aFloat) {
        this.centerPoint = aFloat;
        rectangle.x = centerPoint.x - rectangle.width / 2;
        rectangle.y = centerPoint.y - rectangle.height / 2;
    }
}
