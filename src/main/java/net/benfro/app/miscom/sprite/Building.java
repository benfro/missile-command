package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.sprite.MissileModel.MissileState;
import net.benfro.app.miscom.util.GraphicsUtil;
import net.benfro.app.miscom.util.SpriteBuilder;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author benfro
 */
public class Building extends AbstractGameSprite<BuildingModel> implements ImpactDetectable, Collidible {

    Building(BuildingModel model, Float handle, Rectangle2D.Float rectangle, Color color) {
        super(model, handle, rectangle, color);
    }

    /**
     * Testability purposes
     */
    Building(BuildingModel model, Float centerpoint, int height, int width) {
        super(model, centerpoint, height, width);
    }

    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = GraphicsUtil.toGraphics2D(g);
        g2.setColor(color);
        g2.draw(rectangle);
//        g2.fillRect((int) centerPoint.x, 
//                    (int) centerPoint.y, 
//                    (int) rectangle.getWidth(), 
//                    (int) rectangle.getHeight());
    }

    @Override
    public <T extends AbstractGameSprite> boolean isCollision(T other) {
        boolean isInside = isInside(other.getCenterPoint());
        if(other instanceof MissileSprite){
            return isInside && ((MissileSprite)other).getModel().getState().equals(MissileState.RUNNING);
        }
        return false;
    }

    @Override
    /**
     * Invoked when building are hit by ICBM
     */
    public void onCollision() {
        // Decrease the height by a health fraction
        double partOfHeight = rectangle.getHeight() / getModel().health;
        // Set new height while keeping the width
        setCenterPoint(GraphicsUtil.toPoint(getCenterPoint().x, getCenterPoint().y + partOfHeight/2));
        rectangle.setRect(rectangle.getMinX(), rectangle.getMinY(), rectangle.getWidth(), rectangle.getHeight() - partOfHeight);
        // Lower building with as much centerPointas you have decreased its height
        
        // Decrease health
        getModel().decreaseHealth();
    }


    // TODO: Test me!
    public static class Builder implements SpriteBuilder<Building> {

        private Float handle;

        private int height;

        private int width;

        private int health;

        private Color color;

        public Builder() {
        }
        
        public Builder color(Color color) {
            this.color = color;
            return this;
        }
        
        public Builder height(int height) {
            this.height = height;
            return this;
        }
        public Builder width(int width) {
            this.width = width;
            return this;
        }
        public Builder health(int health) {
            this.health = health;
            return this;
        }
        
        public Builder handle(Point2D.Float handle) {
            this.handle = handle;
            return this;
        }

        public Building build() {
            BuildingModel model = new BuildingModel(health);
            return new Building(model, handle, GraphicsUtil.toRectangle(handle,width,height), color);
        }
    }
}
