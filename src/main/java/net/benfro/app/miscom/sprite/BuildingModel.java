package net.benfro.app.miscom.sprite;

/**
 *
 * @author benfro
 */
public class BuildingModel {

    int health;

    public BuildingModel(int health) {
        this.health = health;
    }

    void decreaseHealth() {
        if(health > 0) {
            health--;
        }
    }

    public int getHealth() {
        return health;
    }
    
}
