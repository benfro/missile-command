package net.benfro.app.miscom.sprite;

/**
 * Implemented by objects that need specified behaviour on collisions
 * @author benfro
 */
public interface Collidible {
    
    /**
     * Define behavour on the event of collosion
     */
    public abstract void onCollision();
 
    
}
