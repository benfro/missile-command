package net.benfro.app.miscom.sprite;

/**
 * Interface to be implemented by classes that need to detect if they have collided with others
 * @author benfro
 */
public interface ImpactDetectable {
    
        /**
     * Investigate if this object have collided with <code>other</code>
     * @param <T>
     * @param other
     * @return 
     */
    public abstract <T extends AbstractGameSprite> boolean isCollision(T other);
    
}
