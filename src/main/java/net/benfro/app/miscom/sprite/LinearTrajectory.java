package net.benfro.app.miscom.sprite;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import net.benfro.app.miscom.util.SpriteBuilder;

import java.awt.geom.Point2D;

/**
 * Value class representing a line from point to point. Immutable.
 */
public class LinearTrajectory {

    private final Point2D.Float startingPoint;

    private final Point2D.Float aimingPoint;

    private final double theta;

    LinearTrajectory(Point2D.Float start, Point2D.Float end) {
        this.startingPoint = start;
        this.aimingPoint = end;
        this.theta = getAngleInRadians();
    }

    public Point2D.Float getStartingPoint() {
        return new Point2D.Float(startingPoint.x, startingPoint.y);
    }

    public Point2D.Float getAimingPoint() {
        return new Point2D.Float(aimingPoint.x, aimingPoint.y);
    }

    public double getTheta() {
        double theta_copy = theta;
        return theta_copy;
    }

    /**
     * Visible for testability
     *
     * @return The angle of travel for this missile
     */
    final double getAngleInRadians() {
        double deltaX = aimingPoint.x - startingPoint.x;
        double deltaY = aimingPoint.y - startingPoint.y;

        double result = Math.atan(deltaY / deltaX);
        // Dealing with the "Arctan problem" [90 > theta > -90]
        // Quadrant I
        if (deltaX > 0 && deltaY > 0) {
            return result;
        }
        // Special case: -90 degrees that should be 270 degrees
        if(deltaX == 0 && deltaY < 0) {
            return (Math.PI / 2) * 3;
        }
        // Quadrant II and III
        if (deltaX < 0) {
            return (result + Math.PI);
        }
        // Quadrant IV
        if (deltaX > 0 && deltaY < 0) {
            return (result + Math.PI * 2);
        }
        return result;
    }

    public static class Builder implements SpriteBuilder<LinearTrajectory> {

        private Point2D.Float startingPoint;

        private Point2D.Float aimingPoint;

        private Optional<java.lang.Float> startX = Optional.fromNullable(null);

        private Optional<java.lang.Float> startY = Optional.fromNullable(null);

        private Optional<java.lang.Float> endX = Optional.fromNullable(null);

        private Optional<java.lang.Float> endY = Optional.fromNullable(null);;

        public Builder() {
        }

        public final Builder startX(float x) {
            startX = Optional.of(x);
            return this;
        }

        public final Builder startY(float y) {
            startY = Optional.of(y);
            return this;
        }

        public final Builder endX(float x) {
            endX = Optional.of(x);
            return this;
        }

        public final Builder endY(float y) {
            endY = Optional.of(y);
            return this;
        }
        
        public final Builder startingPoint(Point2D.Float p) {
            startingPoint = p;
            return this;
        }
        
        public final Builder aimingPoint(Point2D.Float p) {
            aimingPoint = p;
            return this;
        }

        public LinearTrajectory build() {
            if (startX.isPresent() && startY.isPresent()) {
                startingPoint = new Point2D.Float(startX.get(), startY.get());
            } else {
                Preconditions.checkArgument(startingPoint != null, "This builder has no start point set");
            }
            if(endX.isPresent() && endY.isPresent()) {
                aimingPoint = new Point2D.Float(endX.get(), endY.get());
            } else {
                Preconditions.checkArgument(aimingPoint != null, "This builder has no end point set");
            }

            return new LinearTrajectory(startingPoint, aimingPoint);
        }
    }
}