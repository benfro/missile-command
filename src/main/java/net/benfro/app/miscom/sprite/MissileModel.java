package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.util.GraphicsUtil;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

/**
 *
 * @author benfro
 */
public class MissileModel{

    public static enum MissileState {

        RUNNING,
        EXPLODING,
        SPENT

    }
    /*
     * Number of frames that is to be shown during explosion
     */
    private int explodingCycles;

    private float speed;

    private float explodingFactor;
    
    private MissileState state;

    private final LinearTrajectory linearTrajectory;

    public MissileModel(Float startingPoint, Float aimingPoint, float speed, float explodingFactor) {
        this(new LinearTrajectory(startingPoint, aimingPoint), speed, explodingFactor);
    }
    
    public MissileModel(LinearTrajectory trajectory, float speed, float explodingFactor) {
        this.speed = speed;
        linearTrajectory = trajectory;
        this.explodingFactor = explodingFactor;
        this.state = MissileState.RUNNING;
    }

    public int getExplodingCycles() {
        return explodingCycles;
    }

    public float getSpeed() {
        return speed;
    }

    public MissileState getState() {
        return state;
    }

    public Float getStartingPoint() {
        return linearTrajectory.getStartingPoint();
    }

    public Float getAimingPoint() {
        return linearTrajectory.getAimingPoint();
    }

    public float getExplodingFactor() {
        return explodingFactor;
    }

    void setExplodingCycles(int height) {
        this.explodingCycles = (int) Math.ceil(explodingFactor * height);
    }
    
    void explode() {
        if (state.equals(MissileState.RUNNING)) {
            state = MissileState.EXPLODING;
        }
    }

//    /**
//     * 
//     * @return true if the distance vector to this missiles aiming point 
//     * is greater than the speed vector
//     */
//    private boolean aimingPointETANotInNextMove() {
//        return currentScreenPosition.distance(linearTrajectory.getAimingPoint()) > speed;
//    }
//
//    boolean moveAndReturnAimingPointReached() {
//        if (state == MissileState.RUNNING) {
//            if (aimingPointETANotInNextMove()) {
//                calculateNewPosition();
//                return false;
//            } else {
//                currentScreenPosition = linearTrajectory.getAimingPoint();
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * Calculate the X- and Y-components of the speed vector at angleInRadians
     */
    public Point2D.Float calculateNewPosition(Point2D.Float currentPosition) {

        double xComponent = speed * Math.cos(linearTrajectory.getTheta());
        double yComponent = speed * Math.sin(linearTrajectory.getTheta());

        double deltaX = (currentPosition.x + xComponent);
        double deltaY = (currentPosition.y + yComponent);

        return GraphicsUtil.toPoint(deltaX, deltaY);
    }

    void decreaseExplodingCycles() {
        if (explodingCycles > 0) {
            explodingCycles--;
        }
    }

    void destroy() {
        this.state = MissileState.SPENT;
    }

    /*
     * Visible for testability
     */
    void setState(MissileState missileState) {
        state = missileState;
    }
}
