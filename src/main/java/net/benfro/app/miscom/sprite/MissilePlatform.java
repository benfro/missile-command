package net.benfro.app.miscom.sprite;

import com.google.common.base.Optional;
import net.benfro.app.miscom.util.Factory;
import net.benfro.app.miscom.util.GraphicsUtil;
import net.benfro.app.miscom.util.SpriteBuilder;
import net.benfro.app.miscom.util.SpriteUtil;

import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author benfro
 */
public class MissilePlatform extends AbstractGameSprite<MissilePlatformModel> {
    
    @Override
    public void onCollision() {
        // No functionality yet
    }
    
    @Override
    public <T extends AbstractGameSprite> boolean isCollision(T other) {
        // No planned functionality
        return false;
    }

    /**
     * Make ACMs to defend yourself!
     */
    public class ACMBuilderFactory implements Factory<MissileSprite.Builder> {
        
        public MissileSprite.Builder newInstance() {
            return new MissileSprite.Builder()
                    .color(Color.WHITE)
                    .startX(MissilePlatform.this.firingFromPoint.x)
                    .startY(MissilePlatform.this.firingFromPoint.y)
                    .speed(6)
                    .explodingFactor(4.0f)
                    .trajectoryColor(Color.CYAN);
        }
    }
    private ACMBuilderFactory missileBuilderFactory;
    
    private Point2D.Float firingFromPoint;
    
    private MissilePlatform(MissilePlatformModel model, Float handle, Color color, Rectangle2D.Float d) {
        super(model, handle, d, color);
        this.color = color;
        this.firingFromPoint = SpriteUtil.currentPosition(handle, d);
        this.missileBuilderFactory = new ACMBuilderFactory();
    }
    
    @Override
    public void draw(Graphics g) {
        Graphics2D g2 = GraphicsUtil.toGraphics2D(g);
        g2.setColor(color);
        g2.setStroke(new BasicStroke(4.0f));
        GeneralPath triangle = new GeneralPath(GeneralPath.WIND_EVEN_ODD, 3);
        Point2D.Float[] corners = calculateGeneralTrianglePath();
        triangle.moveTo(corners[0].x, corners[0].y);
        triangle.lineTo(corners[1].x, corners[1].y);
        triangle.lineTo(corners[2].x, corners[2].y);
        triangle.closePath();
        
        g2.draw(triangle);
        g2.fill(triangle);
    }
    
    public MissileSprite spawnACMissile(Point2D.Float target) {
        MissileSprite.Builder missileBuilder = missileBuilderFactory.newInstance();
        MissileSprite missile = missileBuilder
                .endX(target.x)
                .endY(target.y)
                //                                .startingPoint(GraphicsUtil.toPoint(0, 0))
                .explodingFactor(20)
                .build();
        if (getModel().getNumberOfMissiles() > 0) {
            getModel().decreaseMissileCount();
        }
        return missile;
    }

    /**
     * Visible for testability Calculate three triangle cornes using the given
     * Dimension
     */
    Point2D.Float[] calculateGeneralTrianglePath() {
        Point2D.Float[] out = new Point2D.Float[3];
        out[0] = GraphicsUtil.toPoint(centerPoint.x, centerPoint.y + rectangle.getHeight());
        out[1] = GraphicsUtil.toPoint(centerPoint.x + rectangle.getWidth() / 2, centerPoint.y);
        out[2] = GraphicsUtil.toPoint(centerPoint.x + rectangle.getWidth(),
                centerPoint.y + rectangle.getHeight());
        return out;
    }

    /**
     * Build a MissilePlatform
     */
    public static class Builder implements SpriteBuilder<MissilePlatform> {
        
        private Optional<Color> color = Optional.fromNullable(null);
        
        private Optional<Point2D.Float> handle = Optional.fromNullable(null);
        
        private Optional<Integer> numberOfMissiles = Optional.fromNullable(null);
        
        private Optional<java.lang.Float> height = Optional.fromNullable(null);
        
        private Optional<java.lang.Float> width = Optional.fromNullable(null);
        
        private Optional<Point2D.Float> currentScreenPosition = Optional.fromNullable(null);
        
        private Optional<Rectangle2D.Float> rectangle = Optional.fromNullable(null);
        
        public Builder() {
        }
        
        public Builder color(Color color) {
            this.color = Optional.of(color);
            return this;
        }
        
        public Builder handle(Point2D.Float centerPoint) {
            this.handle = Optional.of(centerPoint);
            return this;
        }
        
        public Builder numberOfMissiles(int numberOfMissiles) {
            this.numberOfMissiles = Optional.of(numberOfMissiles);
            return this;
        }
        
        public Builder rectangle(Rectangle2D.Float rectangle) {
            this.rectangle = Optional.of(rectangle);
            return this;
        }

        /**
         * Provide the height of the enclosing Dimension If a Dimension object
         * already is provided to this Builder, this method will silently be
         * ignored
         *
         * @param height
         * @return This Builder object
         */
        public Builder dimensionHeight(float height) {
            this.height = Optional.of(height);
            return this;
        }

        /**
         * Provide the width of the enclosing Dimension. If a Dimension object
         * already is provided to this Builder, this method will silently be
         * ignored
         *
         * @param width
         * @return This Builder object
         */
        public Builder dimensionWidth(float width) {
            this.width = Optional.of(width);
            return this;
        }
        
        public MissilePlatform build() {
            if (!rectangle.isPresent()) {
                if (width.isPresent() && height.isPresent() && handle.isPresent()) {
                    rectangle = Optional.of(GraphicsUtil.toRectangle(handle.get(), width.get(), height.get()));
                } else {
                    throw new IllegalStateException("No Dimension data in this Builder");
                }
            }
            MissilePlatformModel model =
                    new MissilePlatformModel(numberOfMissiles.or(30), SpriteUtil.currentPosition(handle.get(), rectangle.get()));
            return new MissilePlatform(model, handle.get(), color.or(Color.LIGHT_GRAY), rectangle.get());
            
        }
    }
}
