package net.benfro.app.miscom.sprite;

import java.awt.geom.Point2D.Float;

/**
 *
 * @author benfro
 */
public class MissilePlatformModel {

    private int numberOfMissiles;

    public MissilePlatformModel(int numberOfMissiles, Float currentScreenPosition) {
        this.numberOfMissiles = numberOfMissiles;
    }

    public int getNumberOfMissiles() {
        return numberOfMissiles;
    }

    public void decreaseMissileCount() {
        --numberOfMissiles;
    }
}
