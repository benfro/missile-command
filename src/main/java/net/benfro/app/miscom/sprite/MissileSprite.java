package net.benfro.app.miscom.sprite;

import com.google.common.base.Optional;
import net.benfro.app.miscom.util.GraphicsUtil;
import net.benfro.app.miscom.util.SpriteBuilder;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 * A missile in the game
 *
 * @author benfro
 */
public class MissileSprite extends AbstractGameSprite<MissileModel>
        implements Moveable, Collidible, ImpactDetectable {
    
    private Color trajectoryColor;
    
    private float explodingCycles = 0;
    
    private MissileSprite(MissileModel model, Point2D.Float centerpoint,
            float width, float height, Color color, Color trajectoryColor) {
        super(model, centerpoint, GraphicsUtil.toRectangle(centerpoint, width, height), color);
        this.trajectoryColor = trajectoryColor;
    }
    
    public Color getTrajectoryColor() {
        return trajectoryColor;
    }
    
    @Override
    public void draw(Graphics g) {
        if (isRunning()) {
            drawMissileInternal(g);
            drawTrajectoryInternal(g);
        } else if (isExploding() && explodingCycles > 0) {
            explodingCycles--;
            expand(-1);
            explodeMissileInternal(g);
        } else if (isExploding() && explodingCycles <= 0) {
            getModel().destroy();
        }
    }
    
    private void drawMissileInternal(Graphics g) {
        Graphics2D g2 = GraphicsUtil.toGraphics2D(g);
        g2.setColor(color);
        g2.draw(rectangle);
    }
    
    private void drawTrajectoryInternal(Graphics g) {
        Graphics2D g2 = GraphicsUtil.toGraphics2D(g);
        g2.setColor(trajectoryColor);
        float dash1[] = {10.0f};
        BasicStroke dashed =
                new BasicStroke(1.0f,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                10.0f, dash1, 0.0f);
        g2.setStroke(dashed);
        Line2D line = new Line2D.Float(getModel().getStartingPoint().x,
                getModel().getStartingPoint().y,
                getCenterPoint().x,
                getCenterPoint().y);
        g2.draw(line);
    }
    
    private void explodeMissileInternal(Graphics g) {
        Graphics2D g2 = GraphicsUtil.toGraphics2D(g);
        g2.setColor(color);
        g2.fillOval((int) rectangle.getMinX(),
                (int) rectangle.getMinY(),
                (int) rectangle.getWidth(),
                (int) rectangle.getWidth());
    }
    
    @Override
    public synchronized void move() {
        if (isRunning()) {
            if (moveAndReturnAimingPointReached()) {
                onCollision();
            }
        }
    }
    
    @Override
    /**
     * Set this missile in the EXPLODING state, initialize the explodingCycles
     * and set the exploding size on the rectangle
     */
    public synchronized void onCollision() {
        getModel().explode();
        explodingCycles = (float)(Math.max(rectangle.width, rectangle.height) * getModel().getExplodingFactor());
        scale(getModel().getExplodingFactor());
    }
    
    @Override
    /**
     * A collision from a missiles perspective is the fact that its dimension
     * intersect or contains another sprite TODO: Test contains! Test
     */
    public <T extends AbstractGameSprite> boolean isCollision(T other) {
        // Avoid self refernce
        if (equals(other)) {
            return false;
        }
        Rectangle2D.Float otherRect = other.getRectangle();
        boolean isCollision = getRectangle().intersects(otherRect)
                || getRectangle().contains(otherRect);
        return isCollision;
    }

    /**
     *
     * @return true if the distance vector to this missiles aiming point is
     * greater than the speed vector
     */
    private boolean aimingPointETANotInNextMove() {
        return getCenterPoint().distance(getModel().getAimingPoint()) > getModel().getSpeed();
    }
    
    boolean moveAndReturnAimingPointReached() {
        if (isRunning()) {
            if (aimingPointETANotInNextMove()) {
                setCenterPoint(getModel().calculateNewPosition(centerPoint));
                return false;
            } else {
                setCenterPoint(getModel().getAimingPoint());
                return true;
            }
        }
        return false;
    }
    
    @Override
    public Point2D.Float getCenterPoint() {
        return centerPoint;
    }
    
    public boolean isDestroyed() {
        return getModel().getState().equals(MissileModel.MissileState.SPENT);
    }
    
    public boolean isRunning() {
        return getModel().getState().equals(MissileModel.MissileState.RUNNING);
    }
    
    public boolean isExploding() {
        return getModel().getState().equals(MissileModel.MissileState.EXPLODING);
    }

    /*
     * Testability purposes
     */
    @Override
    protected void setModel(MissileModel missileModelMock) {
        super.setModel(missileModelMock);
    }
    
    public void expand(double d) {
        rectangle.height += d;
        rectangle.width += d;
        rectangle.x -= d / 2;
        rectangle.y -= d / 2;
    }
    
    public void scale(double factor) {
        float oldWidth = rectangle.width;
        float oldHeight = rectangle.height;
        // scale up the w and h
        rectangle.height *= factor;
        rectangle.width *= factor;
        
        rectangle.x -= (rectangle.width - oldWidth) / 2;
        rectangle.y -= (rectangle.height - oldHeight) / 2;
    }

    /**
     * Construct a MissileSprite
     */
    public static class Builder implements SpriteBuilder<MissileSprite> {
        
        private Optional<java.lang.Float> speed = Optional.fromNullable(null);
        
        private Optional<Color> color = Optional.fromNullable(null);
        
        private Optional<Color> trajectoryColor = Optional.fromNullable(null);
        
        private Optional<java.lang.Float> explodingFactor = Optional.fromNullable(null);
        
        private LinearTrajectory.Builder trajectoryBuilder;
        
        private Optional<java.lang.Float> width = Optional.fromNullable(null);
        
        private Optional<java.lang.Float> height = Optional.fromNullable(null);

        /**
         * Default values
         */
        public Builder() {
            trajectoryBuilder = new LinearTrajectory.Builder();
        }
        
        public Builder startX(int x) {
            trajectoryBuilder.startX(x);
            return this;
        }
        
        public Builder startY(int y) {
            trajectoryBuilder.startY(y);
            return this;
        }
        
        public Builder startX(float x) {
            trajectoryBuilder.startX(x);
            return this;
        }
        
        public Builder startY(float y) {
            trajectoryBuilder.startY(y);
            return this;
        }
        
        public Builder endX(int x) {
            trajectoryBuilder.endX(x);
            return this;
        }
        
        public Builder endY(int y) {
            trajectoryBuilder.endY(y);
            return this;
        }
        
        public Builder endX(float x) {
            trajectoryBuilder.endX(x);
            return this;
        }
        
        public Builder endY(float y) {
            trajectoryBuilder.endY(y);
            return this;
        }
        
        public Builder startingPoint(Point2D.Float p) {
            trajectoryBuilder.startingPoint(p);
            return this;
        }
        
        public Builder endingPoint(Point2D.Float p) {
            trajectoryBuilder.aimingPoint(p);
            return this;
        }
        
        public Builder speed(float speed) {
            this.speed = Optional.of(speed);
            return this;
        }
        
        public Builder trajectoryColor(Color color) {
            this.trajectoryColor = Optional.of(color);
            return this;
        }
        
        public Builder color(Color color) {
            this.color = Optional.of(color);
            return this;
        }
        
        public Builder explodingFactor(float factor) {
            this.explodingFactor = Optional.of(factor);
            return this;
        }
        
        public Builder width(float w) {
            this.width = Optional.of(w);
            return this;
        }
        
        public Builder height(float h) {
            this.height = Optional.of(h);
            return this;
        }
        
        public MissileSprite build() {
            MissileModel model = new MissileModel(trajectoryBuilder.build(), speed.or(3f), explodingFactor.or(2.5f));
            return new MissileSprite(
                    model,
                    model.getStartingPoint(),
                    width.or(1f),
                    height.or(1f),
                    color.or(Color.RED),
                    trajectoryColor.or(Color.ORANGE));
        }
    }
}
