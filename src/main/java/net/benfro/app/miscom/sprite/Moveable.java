package net.benfro.app.miscom.sprite;

/**
 * Implementations should be able to move in a graphic environment.
 * @author benfro
 */
public interface Moveable {
    
    /**
     * Perform calculations and operations to move 
     * graphic representation of implemented class
     * on screen
     */
    public abstract void move();
    
}
