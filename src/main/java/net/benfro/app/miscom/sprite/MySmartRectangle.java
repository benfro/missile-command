package net.benfro.app.miscom.sprite;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author benfro
 */
public class MySmartRectangle extends Rectangle2D.Float {

    private Point2D.Float centerPoint;

    public Point2D.Float getCenterPoint() {
        return centerPoint;
    }

    public void setCenterPoint(Point2D.Float centerPoint) {
        this.centerPoint = centerPoint;
    }
}
