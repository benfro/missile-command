package net.benfro.app.miscom.util;

import java.awt.geom.Point2D;
import java.util.Random;

/**
 * This class generates random points within a rectangle
 * bounded by two Point2D
 * @author benfro
 */
public final class BoundedRandomPointGenerator {
    
    private final Random generator = new Random();
    
    /**
     * Generates a random within the interval 
     */
    private float generateInInterval(float min, float max) {
        if(min > max) {
            throw new IllegalStateException("Min cannot be larger than max");
        }
        return (float) (generator.nextDouble() * max + min);
    }
    
    public final Point2D generateTarget(Point2D.Float upperLeft, Point2D.Float netherRight) {
        float x = generateXCoordinate(upperLeft, netherRight);
        float y = generateYCoordinate(netherRight, upperLeft);
        return new Point2D.Float(x,y);
    }    

    public final float generateXCoordinate(Point2D.Float upperLeft, Point2D.Float netherRight) {
        return generateInInterval(upperLeft.x, netherRight.x);
    }
    
    public final float generateXCoordinate(float min, float max) {
        return generateInInterval(min, max);
    }

    public final float generateYCoordinate(Point2D.Float netherRight, Point2D.Float upperLeft) {
        return generateInInterval(netherRight.y, upperLeft.y);
    }
    
    public final float generateYCoordinate(float min, float max) {
        return generateInInterval(min, max);
    }
}
