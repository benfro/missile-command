package net.benfro.app.miscom.util;

/**
 * Facory interface.
 * @author benfro
 */
public interface Factory<T> {
    
    public abstract T newInstance();
    
}
