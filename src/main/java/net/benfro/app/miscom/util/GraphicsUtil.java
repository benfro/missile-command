package net.benfro.app.miscom.util;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author benfro
 */
public class GraphicsUtil {

    public static Graphics2D toGraphics2D(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        return g2;
    }
    
    public static Point2D.Float toPoint(int x, int y) {
        return new Point2D.Float((float) x, (float) y);
    }
    
    public static Point2D.Float toPoint(float x, int y) {
        return new Point2D.Float( x, (float) y);
    }
    
    public static Point2D.Float toPoint(int x, float y) {
        return new Point2D.Float((float) x, y);
    }
    
    public static Point2D.Float toPoint(double x, double y) {
        return new Point2D.Float((float) x, (float) y);
    }
    
    public static Rectangle2D.Float toRectangle(Point2D.Float centerPoint, float width, float height) {
        Point2D.Float handle = toAwtShapeHandle(centerPoint, width, height);
        return new Rectangle2D.Float(handle.x, handle.y, width, height);
    }
    
    public static Rectangle2D.Float toRectangle(Point2D.Float centerPoint, int width, int height) {
        Point2D.Float handle = toAwtShapeHandle(centerPoint, width, height);
        return new Rectangle2D.Float(handle.x, handle.y, (float)width, (float)height);
    }
    
    // Calculate the AWT handle point from a center point value and widt and heigth
    private static Point2D.Float toAwtShapeHandle(Point2D.Float centerPoint, float width, float height) {
        return toPoint(centerPoint.x - (width/2), centerPoint.y - (height/2));
    }
}
