package net.benfro.app.miscom.util;

/**
 * Interface to be implemented by classes realising the builder
 * pattern
 * @author benfro
 */
public interface SpriteBuilder<S> {
    
    /**
     * Construct an instance of this sprite class
     * @return An instance of S
     */
    public S build();
    
}
