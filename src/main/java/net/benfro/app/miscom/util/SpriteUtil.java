package net.benfro.app.miscom.util;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author benfro
 */
public class SpriteUtil {
    
    /**
     * Return the 'logical' center of a sprite.
     * @param handle The upper left corner of the enclosing Dimension
     * @param rectangle The  Dimension that encloses the object
     * @return 
     */
    public static Point2D.Float currentPosition(Point2D.Float handle, Rectangle2D.Float rectangle) {
        return new Point2D.Float(
                (float)(handle.x + rectangle.getWidth()/2), 
                (float)(handle.y +  rectangle.getHeight()/2)
                );
    }
    
}
