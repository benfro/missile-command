package net.benfro.app.miscom;

import net.benfro.app.miscom.sprite.AbstractGameSprite;
import net.benfro.app.miscom.sprite.MissileSprite;
import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

/**
 *
 * @author benfro
 */
public class MissileCommandTest {

    private MissileCommand instance;

    @Before
    public void setUp() {
        instance = new MissileCommand();
    }

    @Test
    public void testSpriteListNotNullOnInit() {
        assertNotNull(instance.getStaticSprites());
    }

    @Test
    public void testSpriteListNotEmptyAfterInsert() {
        instance.addStaticSprite(new MockSprite());
        assertTrue(instance.getStaticSprites().size() == 1);
    }
   


    
    @Test
    public void testCollisionDetectInvokesOnCollisionMethodWhenRectanglesIntersectAndMissileIsExploding() {
        List<MissileSprite> icbm = new ArrayList<MissileSprite>();
        List<MissileSprite> acm = new ArrayList<MissileSprite>();
        
        MissileSprite acmMissile = buildACMAt(0, 0, 2);
        acmMissile.onCollision();
        
        MissileSprite icbmMissile = buildACMAt(4, 4, 2);
        MissileSprite icbmMock = spy(icbmMissile);
        
        icbm.add( icbmMock );
        acm.add( acmMissile );
        
        instance.collisionDetectMissiles(icbm, acm);
        
        verify(icbmMock).onCollision();
    }
    
    @Test
    public void testCollisionDetectDoesNotInvokesOnCollisionWhenACMIsNotExploding() {
        List<MissileSprite> icbm = new ArrayList<MissileSprite>();
        List<MissileSprite> acm = new ArrayList<MissileSprite>();
        
        MissileSprite acmMissile = buildACMAt(0, 0, 5);
        
        MissileSprite icbmMissile = buildACMAt(1, 1, 5);
        MissileSprite icbmMock = spy(icbmMissile);
        
        icbm.add( icbmMock );
        acm.add( acmMissile );
        
        instance.collisionDetectMissiles(icbm,acm);
        
        verify(icbmMock, never()).onCollision();
    }
    
    @Test
    public void testExplodeCascading() {
        List<MissileSprite> runners = new ArrayList<MissileSprite>();
        List<MissileSprite> exploders = new ArrayList<MissileSprite>();
        
        MissileSprite acmMissile = buildACMAt(0, 0, 5);
        exploders.add( acmMissile );
        
        MissileSprite runner1 = buildACMAt(10, 10, 5);
        MissileSprite runner1Mock = spy(runner1);
        runners.add( runner1Mock );
        
        MissileSprite runner2 = buildACMAt(20, 20, 5);
        MissileSprite runner2Mock = spy(runner2);
        runners.add( runner2Mock );
        
        MissileSprite runner3 = buildACMAt(25, 25, 5);
        MissileSprite runner3Mock = spy(runner3);
        runners.add( runner3Mock );
        
        instance.collisionDetectMissiles(runners,exploders);        
        verify(runner1Mock, never()).onCollision();
        verify(runner2Mock, never()).onCollision();
        verify(runner3Mock, never()).onCollision();
        
        // Explode the ACM
        acmMissile.onCollision();
        instance.collisionDetectMissiles(runners,exploders);        
        verify(runner1Mock).onCollision();
        verify(runner2Mock, never()).onCollision();
        verify(runner3Mock, never()).onCollision();
        
        // Cause chained explosion
        instance.collisionDetectMissiles(runners,runners);
        verify(runner1Mock).onCollision();
        verify(runner2Mock).onCollision();
        verify(runner3Mock).onCollision();
    }

    /**
     * Build a missile 20 x 20 at [x,y] with speed = 5
     */
    private MissileSprite buildACMAt(int x, int y, int radius) {
        MissileSprite.Builder b = new MissileSprite.Builder();
        b.speed(5)
         .startX(x)
         .startY(y)
         .width(radius*2)
         .height(radius*2)
         .endX(20)
         .endY(45)
         .explodingFactor(2);
        return b.build();
    }

    private class MockSpriteModel {

        public MockSpriteModel() {
           
        }
    }

    private class MockSprite extends AbstractGameSprite<MockSpriteModel> {

        public MockSprite() {
            super(null, null, null, null);
        }

        @Override
        public void draw(Graphics g) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public void onCollision() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public <T extends AbstractGameSprite> boolean isCollision(T other) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        
    }
}
