package net.benfro.app.miscom.sprite;

import org.junit.Before;
import org.junit.Test;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

import static org.junit.Assert.*;

/**
 *
 * @author benfro
 */
public class AbstractGameSpriteTest {

    private AbstractGameSprite instance;

    @Before
    public void setUp() {
        instance = new AbstractGameSpriteImpl(null, new Point2D.Float(0, 0), 100, 80);
    }

    @Test
    public void testGetCenterPoint() {
        assertEquals(new Point2D.Float(0, 0), instance.getCenterPoint());
    }

    @Test
    public void testDimensionGetsCorrectWidthAndHeight() {
        assertEquals(100, instance.getRectangle().getHeight(), 0.0);
        assertEquals(80, instance.getRectangle().getWidth(), 0.0);
    }
    
    @Test
    public void testRectangleBoundCorrectly() {
        assertEquals(-40, instance.getRectangle().getMinX(), 0.0);
        assertEquals(-50, instance.getRectangle().getMinY(), 0.0);
        assertEquals(40, instance.getRectangle().getMaxX(), 0.0);
        assertEquals(50, instance.getRectangle().getMaxY(), 0.0);
    }

    @Test
    public void testIsInsideWorksCorrectly() {
        assertTrue(instance.isInside(new Point2D.Float(30, 35)));
        assertFalse(instance.isInside(new Point2D.Float(90, 50)));
    }
    
    private static class DummyModel {

        public DummyModel() {
        }
    }

    public class AbstractGameSpriteImpl extends AbstractGameSprite<DummyModel> {

        public AbstractGameSpriteImpl() {
            super(null, null, null, null);
        }

        public AbstractGameSpriteImpl(DummyModel model, Float handle, int height, int width) {
            super(model, handle, height, width);
        }

        public void draw(Graphics g) {
        }

        public void onCollision() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public <T extends AbstractGameSprite> boolean isCollision(T other) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }
}
