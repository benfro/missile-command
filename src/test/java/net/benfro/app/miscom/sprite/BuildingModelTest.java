package net.benfro.app.miscom.sprite;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author benfro
 */
public class BuildingModelTest {
    
    private BuildingModel instance;  
    
    @Before
    public void setUp() {
        instance = new BuildingModel(5);
    }
    
    @Test
    public void testGetHealth() {
        assertEquals(5, instance.getHealth());
    }

    @Test
    public void testDecreaseHealthDecreasesHealth() {
        instance.decreaseHealth();
        assertEquals(4, instance.getHealth());
    }
}
