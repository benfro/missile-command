package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.util.GraphicsUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 *
 * @author benfro
 */
public class BuildingTest {
    
    private Building instance;
    
    @Mock
    private MissileSprite missileMock;
    @Mock
    private MissileModel missileModelMock;
    
    @Before
    public void setUp() {
        BuildingModel model = new BuildingModel(5);
        instance = new Building(model, new Point2D.Float(0,0), 100, 50);
        MockitoAnnotations.initMocks(this);
        
        when(missileMock.getModel()).thenReturn(missileModelMock);
    }

    @Test
    public void testIsCollisionIsTrueIfMissileIsRunning() {
        when(missileMock.getCenterPoint()).thenReturn(new Point2D.Float(20,30));
        when(missileModelMock.getState()).thenReturn(MissileModel.MissileState.RUNNING);
        assertTrue("Should be a collision", instance.isCollision(missileMock));
    }
    
    @Test
    public void testIsCollisionIsFalseIsNotRunning() {
        when(missileMock.getCenterPoint()).thenReturn(new Point2D.Float(30,30));
        when(missileModelMock.getState()).thenReturn(MissileModel.MissileState.EXPLODING);
        assertFalse("Should not be detcted as a collision", instance.isCollision(missileMock));
    }
    
    @Test
    public void testIsCollisionIsFalse() {
        // This missile is way outside this building
        when(missileMock.getCenterPoint()).thenReturn(new Point2D.Float(200,200));
        assertFalse("Should be a miss",instance.isCollision(missileMock));
    }
    
    @Test
    public void testOnCollisionDecreasesHealth() {
        assertEquals(5,instance.getModel().getHealth());
        instance.onCollision();
        assertEquals(4,instance.getModel().getHealth());
    }
    
    @Test
    public void testOnCollisionDecreasesHeight() {
        assertEquals(GraphicsUtil.toRectangle(instance.getCenterPoint(), 50, 100),instance.getRectangle());
        instance.onCollision();
        assertEquals(80,instance.getRectangle().getHeight(),0.1);
    }
    
    @Test
    public void testOnCollisionDecreasesHeightToZeroAndRemainZero() {
        assertEquals(GraphicsUtil.toRectangle(instance.getCenterPoint(), 50, 100),instance.getRectangle());
        instance.onCollision();
        instance.onCollision();
        instance.onCollision();
        instance.onCollision();
        instance.onCollision();
        assertEquals(0,instance.getRectangle().getHeight(),0.1);
    }
}
