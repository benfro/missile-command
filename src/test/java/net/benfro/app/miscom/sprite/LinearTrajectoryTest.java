package net.benfro.app.miscom.sprite;

import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;

public class LinearTrajectoryTest {

    private LinearTrajectory instance;

    @Before
    public void setUp() throws Exception {
        instance = new LinearTrajectory(new Point2D.Float(0,0), new Point2D.Float(20,30));
    }

    @Test
    public void testImmutabilityForPoint() throws Exception {
        Point2D.Float start = instance.getStartingPoint();

        start.x = 12;
        start.y = 14;

        assertEquals(new Point2D.Float(0,0), instance.getStartingPoint());
    }

    @Test
    public void testImmutabilityForTheta() throws Exception {
        double theta = instance.getTheta();

        assertTrue(theta == instance.getTheta());

        theta = 2.983495875;

        assertFalse(theta == instance.getTheta());
    }
    
    
    
    @Test(expected=IllegalArgumentException.class)
    public void testBuilderThrowsWhenNoStartXProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startY(3.4f);
        b.build();
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testBuilderThrowsWhenNoStartYProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(3.4f);
        b.build();
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testBuilderThrowsWhenNoEndXProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(3.4f);
        b.startY(3.4f);
        b.endY(7.3f);
        b.build();
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testBuilderThrowsWhenNoEndYProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(3.4f);
        b.startY(3.4f);
        b.endX(7.3f);
        b.build();
    }
    
    @Test
    public void testBuilderDoBuildWellWithAllCoordinatesProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(2.4f);
        b.startY(3.4f);
        b.endX(6.3f);
        b.endY(7.3f);
        LinearTrajectory build = b.build();
        
        assertEquals(new Point2D.Float(2.4f,3.4f), build.getStartingPoint());
        assertEquals(new Point2D.Float(6.3f,7.3f), build.getAimingPoint());
    }
    
    @Test
    public void testBuilderDoBuildWellWithBothPointsProvided() throws Exception {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startingPoint(new Point2D.Float(3.4f,3.4f));
        b.aimingPoint(new Point2D.Float(7.3f,7.3f));
        LinearTrajectory build = b.build();
        
        assertEquals(new Point2D.Float(3.4f,3.4f), build.getStartingPoint());
        assertEquals(new Point2D.Float(7.3f,7.3f), build.getAimingPoint());
    }
    
//    @Test
//    public void testHandlingOfIllegalDivisor() {
//        // When calculating the angle formula {arctan (dY/dX)} does not allow 
//        // the divisor to be null
//        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
//        b.startX(1f);
//        b.startY(1f);
//        b.endX(1f);
//        b.endY(7f);
//        b.build();
//        
//    }
    
    @Test
    public void testHandlingOfZeroDegrees() {
        // When calculating the angle formula {arctan (dY/dX)} does not allow 
        // the divisor to be null
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(2f);
        b.startY(2f);
        b.endX(7f);
        b.endY(2f);
        LinearTrajectory build = b.build();
        assertEquals(0.0, Math.toDegrees(build.getTheta()), 0.0);
    }
    
    @Test
    public void testHandlingOfNinetyDegrees() {
        // When calculating the angle formula {arctan (dY/dX)} does not allow 
        // the divisor to be null
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(1f);
        b.startY(1f);
        b.endX(1f);
        b.endY(7f);
        LinearTrajectory build = b.build();
        assertEquals(90.0, Math.toDegrees(build.getTheta()), 0.0);
    }
    
    @Test
    public void testHandlingOfOnehundredaneightyDegrees() {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(7f);
        b.startY(1f);
        b.endX(3f);
        b.endY(1f);
        LinearTrajectory build = b.build();
        assertEquals(180.0, Math.toDegrees(build.getTheta()), 0.0);
    }
    
    @Test
    public void testHandlingOfTwohundredandseventyDegrees() {
        LinearTrajectory.Builder b = new LinearTrajectory.Builder();
        b.startX(3f);
        b.startY(7f);
        b.endX(3f);
        b.endY(3f);
        LinearTrajectory build = b.build();
        assertEquals(270.0, Math.toDegrees(build.getTheta()), 0.0);
    }
}
