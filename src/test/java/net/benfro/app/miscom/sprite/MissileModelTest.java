package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.sprite.MissileModel.MissileState;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 *
 * @author benfro
 */
public class MissileModelTest {

    private MissileModel instance;

    @Before
    public void setUp() {
        instance = new MissileModel(new Point2D.Float(0, 0), new Point2D.Float(20, 10),20,4.0f);
    }

    @Test
    public void testDefaultValues() {
        assertNotNull(instance);
        assertEquals(20, instance.getSpeed(), 0.0);
//        assertEquals(new Point2D.Float(0, 0), instance.getCurrentScreenPosition());
    }

    private float vector(float a, float b) {
        return (float) Math.sqrt(a * a + b * b);
    }

    @Test
    public void testMissileExplodeInitializesExplodingCycles() {
        instance = new MissileModel(new Point2D.Float(0, 0), new Point2D.Float(20, 10), vector(20, 10) / 2 + 20, 5.0f);

        assertEquals("Should be 0 prior to explode()",
                0, instance.getExplodingCycles());

        instance.setExplodingCycles(2);

        assertEquals("Should be 10 after explode()",
                10, instance.getExplodingCycles());
    }

    @Test
    public void testMissileExplodeAltersStateOnlyOnce() {
        instance = new MissileModel( new Point2D.Float(0, 0), new Point2D.Float(20, 10),vector(20, 10) / 2 + 20, 4.0f);

        instance.setState(MissileState.EXPLODING);

        instance.explode();

        assertEquals("Should be 0 as explode has no impact on already exploding missiles",
                0, instance.getExplodingCycles());
    }

    

    @Test
    public void testExplodeBehaviour() {
        assertEquals(MissileState.RUNNING, instance.getState());
        instance.explode();
        assertEquals(MissileState.EXPLODING, instance.getState());
    }

   // @Test
   // public void testGetAngle() {
   //     assertEquals(26.5650511, Math.toDegrees(instance.getAngleInRadians()), 0.00001);
   // }

    void printDegrees(float rad, String name) {
        System.out.println(name + " In degrees = " + Math.toDegrees(rad));
    }

    

}
