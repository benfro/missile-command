package net.benfro.app.miscom.sprite;

import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author benfro
 */
public class MissilePlatformModelTest {
    
    private MissilePlatformModel instance;
    
    @Before
    public void setUp() {
        instance = new MissilePlatformModel(4, new Point2D.Float(50,50));
    }

    @Test
    public void testGetNumberOfMissiles() {
        assertEquals(4, instance.getNumberOfMissiles());
    }

    @Test
    public void testDecreaseMissileCount() {
        instance.decreaseMissileCount();
        assertEquals(3, instance.getNumberOfMissiles());
    }
    
    
}
