package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.util.GraphicsUtil;
import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author benfro
 */
public class MissilePlatformTest {

    private MissilePlatform instance;

    @Before
    public void setUp() {
        instance = new MissilePlatform.Builder()
                .rectangle(GraphicsUtil.toRectangle(new Point2D.Float(0, 0), 40, 30))
                .numberOfMissiles(4)
                .handle(new Point2D.Float(0, 0))
                .build();
    }

    @Test
    public void testFireMissileDecreaseTheNumberOfMissiles() {
        instance.spawnACMissile(new Point2D.Float(0, 0));
        assertEquals(3, instance.getModel().getNumberOfMissiles());
    }

    @Test
    public void testFireMissileNumberOfMissilesCannotLessThanZero() {
        // Firing 3 of 4
        instance.spawnACMissile(new Point2D.Float(0, 0));
        instance.spawnACMissile(new Point2D.Float(0, 0));
        instance.spawnACMissile(new Point2D.Float(0, 0));
        // One left
        assertEquals(1, instance.getModel().getNumberOfMissiles());
        // Firing the last
        instance.spawnACMissile(new Point2D.Float(0, 0));
        assertEquals(0, instance.getModel().getNumberOfMissiles());
        // Firing empty barrel
        instance.spawnACMissile(new Point2D.Float(0, 0));
        // Should not be negative
        assertEquals(0, instance.getModel().getNumberOfMissiles());

    }

    @Test
    public void testBuilderIgnoresWidthAndHeightProperiesIfDimensionIsSet() {
        Rectangle2D.Float d = GraphicsUtil.toRectangle(new Point2D.Float(0,0),30, 40);
        MissilePlatform platform = new MissilePlatform.Builder()
                .rectangle(d)
                .dimensionHeight(1000)
                .dimensionWidth(2000)
                .handle(new Point2D.Float(0,0))
                .build();

        assertEquals(d, platform.getRectangle());

    }

    @Test
    public void testBuilderIgnoresWidthAndHeightProperiesIfDimensionIsSetReverseInvocationOrder() {
        Rectangle2D.Float d = GraphicsUtil.toRectangle(new Point2D.Float(0,0),30, 40);
        MissilePlatform platform = new MissilePlatform.Builder()
                .dimensionHeight(1000)
                .dimensionWidth(2000)
                .rectangle(d)
                .handle(new Point2D.Float(0,0))
                .build();

        assertEquals(d, platform.getRectangle());
    }
    
    @Test
    public void testBuilderTakesHeightAndWidthIntoDimension() {
        Rectangle2D.Float d = GraphicsUtil.toRectangle(new Point2D.Float(0,0),2000, 1000);
        MissilePlatform platform = new MissilePlatform.Builder()
                .dimensionHeight(1000)
                .dimensionWidth(2000)
                .handle(new Point2D.Float(0,0))
                .build();

        assertEquals(d, platform.getRectangle());
    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilderThrowsWhenNoHandleIsSet() {
         new MissilePlatform.Builder()
                .rectangle(GraphicsUtil.toRectangle(new Point2D.Float(0,0),2000, 1000))
                .build();
     }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilderThrowsExceptionIfNoDimensionAndNoHeightIsSet() {
        new MissilePlatform.Builder()
                .dimensionWidth(2000)
                .build();

    }
    
    @Test(expected=IllegalStateException.class)
    public void testBuilderThrowsExceptionIfNoDimensionAndNoWidthIsSet() {
        new MissilePlatform.Builder()
                .dimensionHeight(1000)
                .build();

    }
    
    @Test
    public void testCalculateGeneralTringlePath() {
        
        MissilePlatform platform = new MissilePlatform.Builder()
                .dimensionHeight(40)
                .dimensionWidth(30)
                .handle(new Point2D.Float(50,50))
                .build();
        
        // Fixtures
        Point2D.Float netherLeftTriangleCorner = new Point2D.Float(50,90);
        Point2D.Float topTriangleCorner = new Point2D.Float(65,50);
        Point2D.Float netherRightTriangleCorner = new Point2D.Float(80,90);
        
        Point2D.Float[] points = platform.calculateGeneralTrianglePath();
        
        assertEquals(points[0], netherLeftTriangleCorner);
        assertEquals(points[1], topTriangleCorner);
        assertEquals(points[2], netherRightTriangleCorner);
    }
    
    @Test
    public void testSomething() {
        MissileSprite missile = instance.spawnACMissile(new Point2D.Float(22, 33));
        assertEquals(22,missile.getModel().getAimingPoint().x, 0.0);
        assertEquals(33,missile.getModel().getAimingPoint().y, 0.0);
    }
}
