package net.benfro.app.miscom.sprite;

import net.benfro.app.miscom.sprite.MissileModel.MissileState;
import net.benfro.app.miscom.sprite.MissileSprite.Builder;
import net.benfro.app.miscom.util.GraphicsUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;

/**
 *
 * @author benfro
 */
public class MissileSpriteTest {
    
    private MissileSprite instance;
    
    private MissileSprite.Builder builder;

    @Before
    public void setUp() {
        builder = new MissileSprite.Builder().startX(0).startY(0).endX(1).endY(1);
    }
    
    @Test
    public void testRectangeleCenterPointVersusHandle() {
        // In a missile sprite, the centerpoint is the important thing
        // The handle is calculated from it
        instance = builder.width(20).height(30).build();
        // 
        assertEquals(-10, instance.getRectangle().getMinX(), 0);
        assertEquals(-15, instance.getRectangle().getMinY(), 0);
        assertEquals(10, instance.getRectangle().getMaxX(), 0);
        assertEquals(15, instance.getRectangle().getMaxY(), 0);
        
        instance.setCenterPoint(new Point2D.Float(5,5));
        
        assertEquals(-5, instance.getRectangle().getMinX(), 0);
        assertEquals(-10, instance.getRectangle().getMinY(), 0);
        assertEquals(15, instance.getRectangle().getMaxX(), 0);
        assertEquals(20, instance.getRectangle().getMaxY(), 0);
    }

    @Ignore 
    @Test
    public void testOnCollision() {
        instance.onCollision();
        assertEquals(MissileState.EXPLODING, instance.getModel().getState());
    }
    
    @Test
//    @Ignore
    public void testDimensionIsNotNullOnCreation() {
        instance = builder.width(3).build();                
        assertNotNull(instance.getRectangle());
    }
    
    @Test
    public void testDimensionIsBiggerAfterOnCollision() {
        instance = builder
                .width(2)
                .height(2)
                .explodingFactor(3.0f)
                .startingPoint(new Point2D.Float(0,0))
                .build();                
        instance.onCollision();
        assertEquals(GraphicsUtil.toRectangle(instance.getCenterPoint(),6,6), instance.getRectangle());
    }
    
    @Test
    public void testExpand() {
        instance = builder
                .width(2)
                .height(2)
                .startingPoint(new Point2D.Float(0,0))
                .build();
        assertEquals(-1, instance.getRectangle().getMinX(), 0);
        assertEquals(-1, instance.getRectangle().getMinY(), 0);
        assertEquals(1, instance.getRectangle().getMaxX(), 0);
        assertEquals(1, instance.getRectangle().getMaxY(), 0);
        
        instance.expand(4.0);
        
        assertEquals(-3, instance.getRectangle().getMinX(), 0);
        assertEquals(-3, instance.getRectangle().getMinY(), 0);
        assertEquals(3, instance.getRectangle().getMaxX(), 0);
        assertEquals(3, instance.getRectangle().getMaxY(), 0);
    }
    
    @Test
    public void testScale() {
        instance = builder
                .width(2)
                .height(2)
                .startingPoint(new Point2D.Float(0,0))
                .build();
        assertEquals(-1, instance.getRectangle().getMinX(), 0);
        assertEquals(-1, instance.getRectangle().getMinY(), 0);
        assertEquals(1, instance.getRectangle().getMaxX(), 0);
        assertEquals(1, instance.getRectangle().getMaxY(), 0);
        
        instance.scale(3.0);
        
        assertEquals(-3, instance.getRectangle().getMinX(), 0);
        assertEquals(-3, instance.getRectangle().getMinY(), 0);
        assertEquals(3, instance.getRectangle().getMaxX(), 0);
        assertEquals(3, instance.getRectangle().getMaxY(), 0);
        assertEquals(6, instance.getRectangle().width, 0);
        assertEquals(6, instance.getRectangle().height, 0);
        assertEquals(new Point2D.Float(0,0), instance.getCenterPoint());
    }
    
    @Test
    public void testIsCollisionShouldBeTrue() {
        instance = builder
                .startX(10)
                .startY(10)
                .endX(0)
                .endY(0)
                .width(10)
                .build();  
        MissileSprite other = new Builder()
                .startX(10)
                .startY(10)
                .endX(10)
                .endY(20)
                .width(10)
                .build(); 
        
        assertTrue(instance.isCollision(other));
        assertTrue(other.isCollision(instance));
        
    }
    
    private float vector(float a, float b) {
        return (float) Math.sqrt(a * a + b * b);
    }
    
    private MissileSprite.Builder getBuilder(int x1, int y1, int x2, int y2, float speed) {
        return new MissileSprite.Builder()
                .startX(x1)
                .startY(y1)
                .endX(x2)
                .endY(y2)
                .speed(speed);
    }
    
    @Test  //@Ignore
    public void testMoveDoesNotMoveStartingPoint() {
        instance = getBuilder(0,0,20,10,vector(20, 10)).build();
        instance.moveAndReturnAimingPointReached();
        assertEquals(new Point2D.Float(0, 0), instance.getModel().getStartingPoint());
    }

    @Test
    public void testMoveIsReflectedCorrectlyInCurrentPosition() {
        instance = getBuilder(0,0,20,10,vector(20, 10))
                .startingPoint(new Point2D.Float(0,0)).build();

        assertTrue(instance.getCenterPoint().equals(new Point2D.Float(0, 0)));
        instance.moveAndReturnAimingPointReached();
        assertFalse(instance.getCenterPoint().equals(new Point2D.Float(0, 0)));

        assertEquals(20, instance.getCenterPoint().x, 0.4);
        assertEquals(10, instance.getCenterPoint().y, 0.4);
    }

    @Test
    public void testAnglesBiggerThanNinetyIsNotPossibleXAxis() {
        instance = getBuilder(40,0,0,40,vector(40, 40)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(0, instance.getCenterPoint().x, 0.4);
    }

    @Test //@Ignore
    public void testAnglesBiggerThanNinetyIsNotPossibleYAxis() {
        instance = getBuilder(40,0,0,40,vector(40, 40)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(40, instance.getCenterPoint().y, 0.5);
    }

    @Test //@Ignore
    public void testAnglesBiggerThanNinetyIsNotPossibleSmallerAngleXAxis() {
        instance = getBuilder(40,0,0,50,vector(50, 40)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(0, instance.getCenterPoint().x, 0.4);
    }

    @Test //@Ignore
    public void testAnglesBiggerThanNinetyIsNotPossibleSmallerAngleYAxis() {
        instance = getBuilder(40,0,0,50,vector(50, 40)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(50, instance.getCenterPoint().y, 0.4);
    }

    @Test //@Ignore
    public void testExplodingStateInhibitsMove() {
        instance = getBuilder(0,0,20,10,vector(20, 10)).build();
        instance.getModel().explode();
        assertTrue(instance.getCenterPoint().equals(new Point2D.Float(0, 0)));
        instance.moveAndReturnAimingPointReached();
        assertTrue(instance.getCenterPoint().equals(new Point2D.Float(0, 0)));
    }

    @Test //@Ignore
    public void testMissileDoesNotMoveBeyondAimingPoint() {
        // speed is set to a slightly higher value than half the distance
        instance = getBuilder(0,0,20,10,vector(20, 10) / 2 + 20)
                .build();
        // Two moves should get it to aiming point despite the fact that speed * 2 is beyond
        instance.moveAndReturnAimingPointReached();
        instance.moveAndReturnAimingPointReached();

        assertEquals("Should not go further than aiming point",
                new Point2D.Float(20, 10), instance.getCenterPoint());
    }
    
    @Test //@Ignore
    public void testAngleAndMoveInComputerQuadrantOne() {
        instance = getBuilder(0,0,30,20,vector(30, 20))
                .build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(30, instance.getCenterPoint().x,0.4);
        assertEquals(20, instance.getCenterPoint().y, 0.4);
    }

    @Test //@Ignore
    public void testAngleAndMoveInComputerQuadrantFour() {
        instance = getBuilder(0,0,30,-20,vector(30, 20)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(30, instance.getCenterPoint().x, 0.4);
        assertEquals(-20, instance.getCenterPoint().y, 0.4);
    }

    @Test //@Ignore
    public void testAngleInComputerQuadrantThree() {
        instance = getBuilder(0,0,-30,-20,vector(-30, -20)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(-30, instance.getCenterPoint().x, 0.4);
        assertEquals(-20, instance.getCenterPoint().y, 0.4);
    }

    @Test //@Ignore
    public void testAngleInComputerQuadrantTwo() {
        instance = getBuilder(0,0,-30,20,vector(-30, 20)).build();

        instance.moveAndReturnAimingPointReached();

        assertEquals(-30, instance.getCenterPoint().x, 0.4);
        assertEquals(20, instance.getCenterPoint().y, 0.4);
    }
    
    @Test //@Ignore
    public void testSomeMethod() {
        instance = getBuilder(0,0,30,20,5)
                .width(40)
                .height(50)
                .build();
        Point2D.Float expectedCurrPos = new Point2D.Float(0,0);
        assertEquals(expectedCurrPos, instance.getCenterPoint());
    }

}
