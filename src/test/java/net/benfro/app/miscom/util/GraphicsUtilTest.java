/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.benfro.app.miscom.util;

import org.junit.Before;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D.Float;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author benfro
 */
public class GraphicsUtilTest {
    
    public GraphicsUtilTest() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void testToRectangleFromcenterPoint() {
        Float toRectangle = GraphicsUtil.toRectangle(new Point2D.Float(10,15), 20, 30);
        Point2D.Float fixture = new Point2D.Float(0,0);
        assertEquals(fixture.x, (float)toRectangle.getMinX(), 0 );
        assertEquals(fixture.y, (float)toRectangle.getMinY(), 0 );
    }

   
}
